from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Meetup, Participant
from .forms import RegisterForm
# Create your views here.


def welcome_page(request):
    meetups = Meetup.objects.all()
    # we want to return a response here, when we have a request for a certain url
    return render(request, 'meetups/index.html', {'meetups': meetups})


def meetup_details(request, meetup_slug):
    try:
        selected_meetup = Meetup.objects.get(slug=meetup_slug)
        if request == 'GET':
            registration_form = RegisterForm()
        else:
            registration_form = RegisterForm(request.POST)
            if registration_form.is_valid():
                user_email = registration_form.cleaned_data['email']
                participant, _ = Participant.objects.get_or_create(email=user_email)
                selected_meetup.participants.add(participant)
                return redirect('confirm-registration', meetup_slug=meetup_slug)
        return render(request, 'meetups/meetup_details.html', {'meetup_found': True,
                                                               'meetup': selected_meetup,
                                                               'form': registration_form,
                                                               'organizer_email': selected_meetup.organizer_email})
    except Exception as ex:
        print(ex)
        return render(request, 'meetups/meetup_details.html', {
            'meetup_found': False
        })


def confirm_registration(request, meetup_slug):
    meetup = Meetup.objects.get(slug=meetup_slug)
    return render(request, 'meetups/registration-success.html',
                  {'organizer_email': meetup.organizer_email})