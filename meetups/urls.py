from django.urls import path
from . import views

urlpatterns = [
    path('meetups/', views.welcome_page, name='all-meetings'),
    path('meetups/<slug:meetup_slug>/success', views.confirm_registration, name='confirm-registration'),  # our-domain.com/meetups
    path('meetups/<slug:meetup_slug>', views.meetup_details, name='meetup-detail'),
    # our-domain.com/meetups/first or second meeting
]

